const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
//const HtmlMinifierPlugin = require('html-minifier-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const autoprefixer = require('autoprefixer');
const extractLess = new ExtractTextPlugin({
  filename: "index.css",
  disable: process.env.NODE_ENV === "development"
});

module.exports = (env) => {
  const config = {
    entry: ['@babel/polyfill', './src/index.jsx'],
    output: {
      path: path.resolve(__dirname, 'public'),
      publicPath: '',
      filename: 'main.js'
    },
    devServer: {
      contentBase: './public',
      historyApiFallback : true,
      disableHostCheck: true,
    },
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.css?$/,
          use: [
            "style-loader",
            {
              loader: "css-loader",
              options: { minimize: true }
            },
            "postcss-loader"
          ]
        },
        {
          test: /\.less$/,
          use: [
            "style-loader",
            {
              loader: "css-loader",
              options: { minimize: true }
            },
            "less-loader", "postcss-loader"
          ],
          // use: extractLess.extract({
          //   fallback: "style-loader",
          //   use: ["css-loader", "less-loader", "postcss-loader"]
          // })
        },
        {
          test: /\.(gif|png|jpe?g|svg)$/i,
          use:[
            "file-loader",
          ]
        },
        {
          test: /\.(ttf|otf|eot|woff|woff2|ico)$/,
          use:[{
            loader: "file-loader",
            options:{
              name: "[path][name].[ext]"
            }
          }]
        }//loader
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        inject: 'body',
        hash: true,
        template: './src/index.html',
        filename: 'index.html',
        minify: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          minifyJS: true,
          minifyCSS: true,
          removeAttributeQuotes: true,
          removeComments: true,
          removeEmptyAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true,
        },
      }),
      new ScriptExtHtmlWebpackPlugin({
        defaultAttribute: 'async',
      }),
      extractLess,
      autoprefixer,
    ]
  };
  if (env.NODE_ENV === 'prod') {
    config.plugins.push(new OptimizeCssAssetsPlugin());
    config.plugins.push(new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false,
      }),
    );
    config.optimization = {
      minimizer: [new UglifyJsPlugin({
        extractComments: 'all',
        sourceMap: true,
        uglifyOptions: {
          ecma: 8,
          compress: true,
          mangle: true,
          toplevel: true,
          keep_classnames: false,
          keep_fnames: false,
        },
      }),
      ]
    }
  }
  return config;
};

export default [
  {
    id: 'link1',
    name: 'News',
    href: '#'
  },
  {
    id: 'link2',
    name: 'Overview',
    href: '#'
  },
  {
    id: 'link3',
    name: 'Tutorials',
    href: '#'
  },
  {
    id: 'link4',
    name: 'FAQ',
    href: '#'
  },
  {
    id: 'link5',
    name: 'Blog',
    href: '#'
  },
  {
    id: 'link6',
    name: 'Design',
    href: '#'
  },
  {
    id: 'link7',
    name: 'Resources',
    href: '#'
  },
  {
    id: 'link8',
    name: 'Terms',
    href: '#'
  },
  {
    id: 'link9',
    name: 'Partners',
    href: '#'
  },
  {
    id: 'link10',
    name: 'Code',
    href: '#'
  },
  {
    id: 'link11',
    name: 'Guides',
    href: '#'
  },
  {
    id: 'link12',
    name: 'Conditions',
    href: '#'
  },
  {
    id: 'link13',
    name: 'Shop',
    href: '#'
  },
  {
    id: 'link14',
    name: 'Collaborate',
    href: '#'
  },
  {
    id: 'link15',
    name: 'Examples',
    href: '#'
  },
  {
    id: 'link16',
    name: 'Help',
    href: '#'
  }
]
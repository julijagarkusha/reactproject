export default [
  {
    id: 'menuItem1',
    name: 'About me',
    linkTo: '#singUp'
  },
  {
    id: 'menuItem2',
    name: 'Relationships',
    linkTo: '#aboutMe'
  },
  {
    id: 'menuItem3',
    name: 'Requirements',
    linkTo: '#requirements'
  },
  {
    id: 'menuItem4',
    name: 'Users',
    linkTo: '#users'
  },
  {
    id: 'menuItem5',
    name: 'Sign Up',
    linkTo: '#register'
  }
]
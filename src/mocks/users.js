import userNoah from '../assets/imgs/user-noah-2x.jpg';
import userAdolph from '../assets/imgs/user-adolph-2x.png';
import userSmith from '../assets/imgs/user-liam-2x.jpg';
import userElizabeth from '../assets/imgs/user-elizabeth-2x.jpg';
import userAlex from '../assets/imgs/user-alexander-2x.jpg';
import userMason from '../assets/imgs/user-mason-2x.jpg';

export default [
  {
    id: 'user1',
    name: 'Noah',
    position: 'Leading specialist of the Control Department',
    email: 'noah.controldepartment@gmail...',
    phone: '+38 (050) 678 03 24',
    src: userNoah
  },
  {
    id: 'user2',
    name: 'Adolph Blaine Charles David Earl ',
    position: 'The contextual advertising specialist',
    email: 'adolph.blainecharles-davidearl@...',
    phone: '+38 (095) 556 08 45',
    src: userAdolph
  },
  {
    id: 'user3',
    name: 'Liamgrievescasey Smith Wiam',
    position: 'Lead designer',
    email: 'liamgrievescasey@example.com',
    phone: '+38 (050) 273 93 32',
    src: userSmith
  },
  {
    id: 'user4',
    name: 'Elizabeth',
    position: 'Frontend developer',
    email: 'elisabet.frontend@gmail.com',
    phone: '+38 (095) 924 66 37',
    src: userElizabeth
  },
  {
    id: 'user5',
    name: 'Alexander',
    position: 'Backend developer',
    email: 'alexander.backend@gmail.com',
    phone: '+38 (050) 789 24 09',
    src: userAlex
  },
  {
    id: 'user6',
    name: 'Mason',
    position: 'QA',
    email: 'mason.qa@gmail.com',
    phone: '+38 (095) 283 27 00',
    src: userMason
  }
]
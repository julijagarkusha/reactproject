import React, {PureComponent} from 'react';
import links from '../../../mocks/links.js';
import './links.less'

export default class Links extends PureComponent {
  render() {
    return (
      <ul className='footerInfo__links'>
        {
          links.map((linkItem, key)=>{
            return (
              <li key={`key-${linkItem.id}`}>
                <a href={linkItem.href}>
                  <span>{linkItem.name}</span>
                </a>
              </li>
            )
          })
        }
      </ul>
    )
  }
}
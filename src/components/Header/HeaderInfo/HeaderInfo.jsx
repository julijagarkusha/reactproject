import React, {PureComponent} from 'react';
import './headerInfo.less';

export default class HeaderInfo extends PureComponent {
  render() {
    return (
      <div className='headerInfo' id='signUp'>
        <div className='headerInfo__content'>
          <h1 className='headerInfo__head'>Test assignment for Frontend Developer position</h1>
          <span className='headerInfo__description'>
            We kindly remind you that your test assignment should be submitted as a link to github/bitbucket repository. Please be patient, we consider and respond
            to every application that meets minimum requirements. We look forward to your submission. Good luck!
          </span>
          <a className='headerInfo__signUpButton' role='button' href='#users'>Sing Up</a>
        </div>
      </div>
    )
  }
}
import React, {PureComponent} from 'react';
import Logo from '../Logo/Logo.jsx';
import Menu from '../Menu/Menu.jsx';
import UserAccount from '../UserAccount/UserAccount.jsx';
import HeaderInfo from './HeaderInfo/HeaderInfo.jsx';
import MobileMenu from '../Menu/MobileMenu.jsx';
import './Header.less'

export default class Header extends PureComponent {
  constructor(...props) {
    super (...props);

    this.state = {
      menuSwitch: 'headerContent__links',
      scrollTop: 0,
      headerFixed: false,
    };
  }

  handleScroll = (event) => {
    const scrollTop = (window.pageYOffset !== undefined)
      ? window.pageYOffset
      : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    if (scrollTop > 60 && !this.state.headerFixed) {
      this.setState({
        headerFixed: true,
      });
    }

    if (scrollTop < 60 && this.state.headerFixed) {
      this.setState({
        headerFixed: false,
      });
    }
  };

  menuSwitcher = (event) => {
    const {target} = event;
    this.state.menuSwitch === 'headerContent__links' ?
    this.setState({
      menuSwitch: 'headerContent__linksMobile'
    }) : this.setState({
        menuSwitch: 'headerContent__links'
      })
  };

  menuHidden = (event) => {
    event.target.classList.contains('headerContent__linksMobile') ?
      this.setState({
        menuSwitch: 'headerContent__links'
      }) : ''
  };

  componentDidMount () {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount () {
    window.removeEventListener('scroll', this.handleScroll);
  }
  render() {
    return (
      <header>
        <div className={`headerContent ${this.state.headerFixed ? 'headerContent_fixed' : ''}`}>
          <div className='headerContent__box'>
            <Logo />
            <MobileMenu mobileMenuSwitch={this.menuSwitcher}/>
            <div className={this.state.menuSwitch} onClick={this.menuHidden}>
              <Menu />
              <UserAccount />
            </div>
          </div>
        </div>
        <HeaderInfo />
      </header>
    )
  }
}

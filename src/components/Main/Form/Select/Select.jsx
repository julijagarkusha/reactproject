import React, {PureComponent} from 'react';
import selectValue from '../../../../mocks/selectValue.js';
import ArrowSvg from './ArrowSvg.jsx';
import './select.less'

export default class Select extends PureComponent {
  constructor (...props) {
    super(...props);

    this.state = {
      positions: [],
      optionVisible: true,
      listVisible: false
    }
  }

  handleClick = () => {
    this.setState({
      listVisible: !this.state.listVisible,
    });
  };


  componentDidMount () {
    // fetch('https://mocksvc.mulesoft.com/mocks/2d186155-7b1f-4da4-bc3c-3e200ab468fe/api/v1/positions')
    //   .then(response => {
    //     return response.json();
    // })
    //   .then(data => {
    //     this.setState({
    //       positions: data.positions
    //     });
    // })
  }

  render () {
    return (
        <div className="checkIn__select checkIn__select_visible">
          <div className="select__selectedOption"
               onClick={this.handleClick}>{this.props.selectedValue}
            {this.state.optionVisible && <ArrowSvg />}
          </div>
          {this.state.listVisible && <ul className='select__options'>
            {
              selectValue.map((item) => {
                return <li key={`positions-${item.id}`}
                           className='select__option'
                            onClick={this.handleClick}>
                  <a value={item.name}
                     onClick={this.props.testClick}>{item.name}</a>
                </li>
              })
            }
          </ul>}
        </div>
    )
  }
}
import React, {PureComponent} from 'react';
import Form from '../Form/Form.jsx';
import './register.less'

export default class Register extends PureComponent {
  render () {
    return (
      <section id="register" className="checkIn">
        <h2>Register to get a work</h2>
        <span className="checkIn__info">Attention! After successful registration and alert, update the list of users in the block from the top</span>
        <Form newUser={this.props.newUser}/>
      </section>
    )
  }
}
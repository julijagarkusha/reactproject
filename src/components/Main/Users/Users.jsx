import React, {PureComponent} from 'react';
import users from '../../../mocks/users';
import './users.less';

export default class Users extends PureComponent {
  constructor(...props) {
    super(...props);

    this.state = {
      users: [],
      usersMobile: users.slice(0, 3),
      userMobileCount: 3,
      currentPage: 0,
      totalPages: 0,
      screenWidth: 0
    }
  }

  usersMobile = () => {
    let count = this.state.userMobileCount;
    let arr = this.state.usersMobile;
    this.setState({
      usersMobile: arr.concat(users.slice(count, count+3)),
      userMobileCount: count+3
    });
    console.log('55', this.state.screenWidth);
  };

  getUsers = async (page=1) => {
    // try {
    //   const response = await fetch(`https://mocksvc.mulesoft.com/mocks/2d186155-7b1f-4da4-bc3c-3e200ab468fe/api/v1/users?count=6&page=${page}`);
    //   const data = await response.json();
    //   await this.setState ({
    //     users: [...this.state.users, ...data.users],
    //     currentPage: data.page,
    //     totalPages: data.total_pages
    //   });
    //   console.log('77', this.state.u
    //   console.log('exception', exception);
    // }sers);
    //     // } catch (exception) {
      fetch(`http://api.504080.com/api/v1/positions`)
      .then(response => {
        return response.json();
        console.log(response.json);
      })
      .then(data => {
        this.setState ({
          users: [...this.state.users, ...data.users],
          currentPage: data.page,
          totalPages: data.total_pages
        });

      })
      .catch(exception=>{
        console.log(exception);
      })
  };

  componentDidMount () {
    this.getUsers();
    this.setState({
      screenWidth: screen.width
    })
  }

  render () {
    return (
      <section id='users' className='users'>
        <h2 onClick={this.usersMobile}>Our cheerful users</h2>
        <p className='users__info'>Attention! Sorting users by registration date</p>
        <ul className='userList'>
          {(this.state.screenWidth > 767) ?
            users.map((user)=>{
              return (<li key={user.id}
                          className='user'>
                <img className='user__img' src={user.src} alt={user.name}/>
                <div className='user__info'>
                  <h4>{user.name}</h4>
                  <span className='user__position'>{user.position}</span>
                  <span className='user__email'>{user.email}</span>
                  <span className='user__phone'>{user.phone}</span>
                </div>
              </li>)
            }) :
            this.state.usersMobile.map(user => {
              return (<li key={user.id}
                          className='user'>
                <img className='user__img' src={user.src} alt={user.name}/>
                <div className='user__info'>
                  <h4>{user.name}</h4>
                  <span className='user__position'>{user.position}</span>
                  <span className='user__email'>{user.email}</span>
                  <span className='user__phone'>{user.phone}</span>
                </div>
              </li>)
            })
          }
          <li className="showUsers" role='button' onClick={this.usersMobile}>
            <a onClick={()=>{
              this.getUsers(this.state.currentPage+1);
            }}>Show more</a>
          </li>
        </ul>
      </section>
    )
  }
}
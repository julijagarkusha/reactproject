import React, {PureComponent} from 'react';
import SingUpImg from './SingUpImg.jsx';
import './singUp.less'

export default class SingUp extends PureComponent {
  render () {
    return (
      <section id='singUp' className='singUp'>
        <h2>Let's get acquainted</h2>
        <div className='singUp__content'>
          <div className='singUp__img'>
            <SingUpImg />
          </div>
          <div className='singUp__info'>
            <h3>I am cool frontend developer</h3>
            <p>When real users have a slow experience on mobile, they're much less likely to find what they are looking for or purchase from you in the future.
              For many sites this equates to a huge missed opportunity, especially when more than half of visits are abandoned if a mobile page takes over 3 seconds
              to load.</p>
            <p>
              Last week, Google Search and Ads teams announced two new speed initiatives to help improve user-experience on the web.
            </p>
            <a href="#users" className='singUp__singUpButton' role='button'>Sing Up</a>
          </div>
        </div>
      </section>
    )
  }
}
import React, {PureComponent} from 'react';
import user from '../../mocks/userAccount.js';
import SignOutSvg from './SignOutSvg.jsx';
import './userAccount.less'

export default class UserAccount extends PureComponent {
  render() {
    return (
      <div className='userAccount'>
        <div className='userAccount__userInfo'>
          <span className='userAccount__userName'>{user.name}</span>
          <span className='userAccount__userEmail'>{user.email}</span>
        </div>
        <div className='userAccount__userImg'>
          <img src={user.img} alt={user.name}/>
          <SignOutSvg />
        </div>
      </div>
    )
  }
}
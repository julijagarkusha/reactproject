import React, {PureComponent} from 'react';
import 'reset-css';
import Home from './Home/Home.jsx';

export default class App extends PureComponent {
  render() {
    return (
      <Home/>
    )
  }
}